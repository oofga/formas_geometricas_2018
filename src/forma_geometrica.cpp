#include "forma_geometrica.hpp"
#include <iostream>

FormaGeometrica::FormaGeometrica(){ 
	base = 5.0;
	altura = 3.0;
	tipo = "Retângulo";
}
FormaGeometrica::FormaGeometrica(float base, float altura){
	this->base = base;
	this->altura = altura;
	tipo = "Retângulo";
}
FormaGeometrica::~FormaGeometrica(){
	cout << "Destruindo a forma geometrica"<< endl;
}

void FormaGeometrica::setBase(float base){
	this->base = base;
}
float FormaGeometrica::getBase(){
	return base;
}
void FormaGeometrica::setAltura(float altura){
	this->altura = altura;
}
float FormaGeometrica::getAltura(){
	return altura;
}
void FormaGeometrica::setTipo(string tipo){
	this->tipo = tipo;
}
string FormaGeometrica::getTipo(){
	return tipo;
}
float FormaGeometrica::calculaArea(){
	return base*altura;
}
float FormaGeometrica::calculaPerimetro(){
	return 2*base + 2*altura;
}

