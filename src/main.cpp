#include "forma_geometrica.hpp"
#include <iostream>
#include "triangulo.hpp"

using namespace std;

int main(int argc, char ** argv){
	
	FormaGeometrica forma1;
	FormaGeometrica * forma2 = new FormaGeometrica(8.0, 4.5);

	FormaGeometrica forma3(4.6, 9.2);

	cout << "Forma1: " << forma1.getTipo() << " área: " << forma1.calculaArea() << endl;
        cout << "Forma2: " << forma2->getTipo() << " área: " << forma2->calculaArea() << endl;
	cout << "Forma3: " << forma3.getTipo() << " área: " << forma3.calculaArea() << endl;

	delete forma2;

	Triangulo triangulo1;
cout << "Triângulo1: " << triangulo1.getTipo() << " área: " << triangulo1.calculaArea() << " Perímetro: " << triangulo1.calculaPerimetro() << endl;
	return 0;
}
