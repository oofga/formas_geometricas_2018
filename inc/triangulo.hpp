#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include "forma_geometrica.hpp"
#include <string>

class Triangulo : public FormaGeometrica{

public:
	Triangulo();
	Triangulo(float base, float altura);
	~Triangulo();

	float calculaArea();
	float calculaPerimetro();

};

#endif
